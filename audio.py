import subprocess
from disnake import AudioSource

class Audio (AudioSource):
	def __init__ (self, url, callback):
		self.args = [
			'-reconnect', '1',
			'-reconnect_streamed', '1',
			'-i', url,
			'-af', 'arealtime',
			'-ar', '48000',
			'-ac', '2',
			'-f', 'wav',
			'-hide_banner',
			'-loglevel', 'panic',
			'pipe:1'
		]

		self.callback = callback
		self.ffmpeg = subprocess.Popen (['ffmpeg'] + self.args, stdout = subprocess.PIPE, stderr = None)
		self.buf = b""
		self.packetSize = 3840
		self.killed = False

	def read (self):
		if (self.killed):
			return b"\x00" * self.packetSize
		
		exitCode = self.ffmpeg.poll ()

		if (exitCode == None):
			while (len (self.buf) < self.packetSize * 5 and self.ffmpeg.poll () == None):
				self.buf += self.ffmpeg.stdout.read (self.packetSize)
		elif (exitCode == 0): # Track ended naturally
			self.callback ()
			self.killed = True
		else:
			print ("Exit code: ")
			print (exitCode)

		packet = self.buf[:self.packetSize]
		self.buf = self.buf[self.packetSize:]
		return packet

	def cleanup (self):
		try:
			self.ffmpeg.kill ()
			self.ffmpeg.communicate ()
		except Exception as e:
			print ("Exception while trying to cleanup audio")
			print (e)

	def seek (self, seekstr):
		values = []
		try:
			seekstr = seekstr.split (":")
			for num in seekstr:
				values.append (int (num))
		except ValueError:
			return 1

		try:
			self.ffmpeg.kill ()
		except:
			print ("ffmpeg wasn't killed while seeking")

		values = ":".join (map (str, values))

		self.ffmpeg = subprocess.Popen (['ffmpeg'] + ['-ss', values] + self.args, stdout = subprocess.PIPE, stderr = None)
		self.buf = b""

		return 0

	def is_opus(self):
		return False