import disnake, requests, utils
import random, time, os
from audio import Audio
from yt_dlp import YoutubeDL

class Engine ():
	def __init__ (self):
		self.version = "1.2"
		self.paused = False
		self.repeating = False
		self.ignoreCallback = False
		self.voiceClient = None
		self.musicSource = None
		self.queue = []
		self.searchResults = []
		self.wasPrependingToQueue = False

	def help (self):
		helpMsgs = {
			"ajutor/a": "Afiseaza acest mesaj",
			"play/p[1,i] <https://youtube.com/watch?v=ID>": "Reda o melodie de pe youtube (i pentru a adauga imediat in coada, 1 pentru a selecta automat primul rezultat",
			"skip/s": "Sari peste melodia curenta",
			"seek [[hh:]mm:]ss": "Reda pornind de la un punct din melodie",
			"coada/c": "Afiseaza muzica din coada",
			"pauza": "Pune BOOMBOX pe pauza",
			"continua": "Continua melodia",
			"repeta/r": "Repeta melodia curenta",
			"stop": "Goleste coada si opreste botul",
			"flush": "Flushuieste cache-ul YoutubeDL"
		}

		outMsg = ":loud_sound: B00MB0X **v{}**\n".format (self.version)

		for msg in helpMsgs:
			outMsg += "\t**- {}:** `{}`\n".format (msg, helpMsgs[msg])

		return outMsg

	def search (self, query, addedBy, prependToQueue, feelingLucky):
		self.wasPrependingToQueue = prependToQueue
		if (len (self.queue) == 0): self.playSFX ("spawn")
		
		self.searchResults = getSearchResults(query)
		if (feelingLucky): return self.chooseSearch (1, addedBy, True)

		outMsg = "Rezultate - `Selecteaza unul cu ;;NUMAR`\n"
		for entryNum in range (len (self.searchResults)):
			outMsg += "{}. **{}**\n".format (entryNum + 1, self.searchResults[entryNum]["title"])

		return outMsg

	def chooseSearch (self, opt, addedBy, feelingLucky = False):
		if (len (self.searchResults) == 0): return "Mai intai cauta ceva cu ;;p query"
		else:
			opt -= 1
			if (opt < 0): opt = 0
			if (opt > len (self.searchResults)): opt = len (self.searchResults) - 1

			url = self.searchResults [opt]["url"]
			self.searchResults = []

			if (len (self.queue) == 0 and not feelingLucky): self.playSFX ("choice")
			return self.play (url, addedBy, prependToQueue = self.wasPrependingToQueue)

	def play (self, link, addedBy, prependToQueue):
		self.wasPrependingToQueue = prependToQueue

		if (link[:7] != "http://" and link[:8] != "https://"):
			link = "https://www.youtube.com/watch?v=" + link

		info = getYTData (link)
		if (len (info) == 0): return "URL Invalid: `{}`".format (link)

		songsToAdd = []
		for entry in info:
			songsToAdd.append ({"url": entry["url"], "title": entry["title"], "addedBy": addedBy})

		playNow = (len (self.queue) == 0)
		if (self.wasPrependingToQueue): self.queue = self.queue[0:1] + songsToAdd + self.queue[1:]
		else: self.queue += songsToAdd

		if (playNow):
			self.paused = False
			self.playNextInQueue ()

			if (len (info) == 1): return "Melodia **{}** va fi acum redata.".format (info[0]["title"])
			else: return "**{}** Melodii au fost puse in coada.".format (len (info))
		else:
			if (len (info) == 1): return "Melodia **{}** a fost pusa in coada.".format (info[0]["title"])
			else: return "**{}** Melodii au fost puse in coada.".format (len (info))

	def skip (self, amount):
		if (self.paused): return "Nu pot sari peste muzica cat timp sunt pe pauza."
		if (len (self.queue) == 0): return "Acum nu se reda nimic."
		else:
			try: amount = int (amount)
			except: amount = 1
			if (amount > len (self.queue)): amount = len (self.queue)

			currentTrack = self.queue[0]["title"]
			self.ignoreCallback = True
			self.queue = self.queue[amount - 1:]
			self.popQueue ()
			self.playNextInQueue ()
			self.ignoreCallback = False

			if (amount == 1): return "Sar peste **{}**".format (currentTrack)
			else: return "Sar peste **{} piese**".format (amount)

	def pause (self):
		if (self.paused): return "Sunt deja pe pauza."

		self.playSFX ("pause")
		self.paused = True
		return "Am oprit redarea."

	def resume (self):
		if (not self.paused): return "Nu sunt pe pauza."

		self.voiceClient.stop ()
		self.voiceClient.play (self.musicSource)
		self.paused = False
		return "Continui redarea."

	def repeat (self):
		self.repeating = not self.repeating

		if (self.repeating): return "Voi repeta melodia curenta la infinit."
		else: return "Nu voi mai repeta melodia curenta."

	def stop (self):
		if (len (self.queue) == 0):
			return "BOOMBOX nu reda nimic."

		self.paused = False
		self.queue = []
		self.playSFX ("stop")

		return "Am golit coada."
	
	def seek (self, seekstr):
		if (self.paused): return "Nu pot da seek cat timp sunt pe pauza."
		if (self.musicSource.seek (seekstr) == 0): return "Redau de la: " + seekstr
		else: return "Format seek: hh:mm:ss (ex: 00:02:00)"

	def popQueue (self):
		if (len (self.queue) != 0): self.queue.pop (0)
		if (len (self.queue) == 0): self.playSFX ("empty")

	def playNextInQueue (self):
		if (len (self.queue) == 0): return
		if ("https://" not in self.queue[0]["url"]): self.queue[0]["url"] = getYTData(self.queue[0]["url"])[0]["url"]

		self.voiceClient.stop ()
		self.musicSource = Audio (self.queue[0]["url"], self.trackEnded)
		self.voiceClient.play (self.musicSource)

	def trackEnded (self, exception = None):
		if (self.ignoreCallback): return
		if (not self.repeating): self.popQueue ()
		if (exception != None):
			print ("Track ended with exception:")
			print (exception)

		self.playNextInQueue ()

	def playSFX (self, folder):
		soundFile = random.choice (os.listdir (utils.getPath () + "/ui/" + folder))
		audiosource = disnake.FFmpegOpusAudio (utils.getPath () + "/ui/" + folder + "/" + soundFile)
		self.voiceClient.pause ()
		self.voiceClient.play (audiosource)

	def getQueue (self):
		outMsg = ""

		if (len (self.queue) != 0):
			if (self.repeating): outMsg = "**>> :repeat_one: {}** - `{}`\n"
			else: outMsg = "**>> {}** - `{}`\n"
			outMsg = outMsg.format (self.queue[0]["title"], self.queue[0]["addedBy"])

		for track in self.queue[1:10]:
			outMsg += "- {} - `{}`\n".format (track["title"], track["addedBy"])

		if (len (self.queue) > 10): outMsg += "_{} More_".format (len (self.queue) - 10)
		if (outMsg == ""): return "In acest moment, nu se reda nimic."
		else: return outMsg

def getYTData (link):
	try:
		info = YoutubeDL({"forceurl": True, "extract_flat": True}).extract_info (link, download = False)
		url = ""

		if ("_type" in info):
			data = []
			for vid in info["entries"]:
				if (vid["title"] != "[Private video]"):
					data.append ({"url": vid["id"], "title": vid["title"]})
			return data
		else:
			maxABR = 0
			for fmt in info["formats"]:
				if ("audio only" in fmt["format"]):
					abr = int (fmt["abr"])
					if (abr > maxABR):
						maxABR = abr
						url = fmt["url"]
						
	except: return []

	if (len (url) == 0): return []

	if ("manifest.googlevideo.com" in url):
		print ("Requested video has manifest.googlevideo.com in url")
		r = requests.get (url).text
		url = ""

		if ('id="251"'):
			r = r[r.find ('id="251"'):]
			r = r[r.find ('<BaseURL>') + len ('<BaseURL>'):]
			url = r[:r.find ('</BaseURL>')]

	if (len (url) == 0): return []
	return [{"url": url, "title": info["title"]}]

def getSearchResults(query):
	info = YoutubeDL ({"extract_flat": True}).extract_info (url = "ytsearch7:" + query, download = False)
	data = []

	for vid in info["entries"]:
		if ("title" in vid and "id" in vid):
			data.append ({"url": vid["id"], "title": vid["title"]})

	return data[:5]