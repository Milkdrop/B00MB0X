import os, sys
import disnake, platform, asyncio, threading
from engine import Engine
import utils

client = disnake.Client(intents=disnake.Intents.all())
voiceClients = {}
engines = {}

@client.event
async def on_message (message):
	global voiceClients

	if message.author == client.user:
		return

	ch = message.channel
	inp = message.content
	author = message.author.name + "#" + message.author.discriminator
	guildID = message.channel.guild.id

	if (guildID not in engines):
		engines[guildID] = Engine ()

	try:
		if (inp[:2] != ";;"):
			return

		inp = inp[2:].split ()
		engine = engines[guildID]

		if (inp[0][0].lower () == "p" and inp[0].lower () != "pauza"): # messy...
			if (len (inp) == 1):
				await ch.send ("Foloseste: `;;p http://youtube.com/link`")
				return

			if (message.author.voice != None): channelID = message.author.voice.channel.id
			else: channelID = None

			if (guildID in voiceClients and voiceClients[guildID].channel.id != channelID and channelID != None):
				await voiceClients[guildID].disconnect ()
				voiceClients.pop (guildID, None)

			if (guildID in voiceClients and not voiceClients[guildID].is_connected ()):
				print ("Disconnecting from discord...")
				await voiceClients[guildID].disconnect ()
				voiceClients.pop (guildID, None)

			if (guildID not in voiceClients):
				if (channelID == None):
					await ch.send ("Nu te vad pe niciun canal de voce.")
					return

				voiceClient = await client.get_channel (channelID).connect ()
				print ("Connecting to channel: {}".format (channelID))

				voiceClients[guildID] = voiceClient				
				engine.voiceClient = voiceClient

			prependToQueue = ("i" in inp[0].lower ())
			feelingLucky = ("1" in inp[0].lower ())
			link = " ".join (inp[1:])

			if (link[:7] != "http://" and link[:8] != "https://"):
				await ch.send (engine.search (link, author, prependToQueue, feelingLucky))
			else:
				await ch.send (engine.play (link, author, prependToQueue))

		elif (inp[0].lower () == "ajutor" or inp[0].lower () == "a"):
			await ch.send (engine.help ())

		elif (inp[0].lower () == "restart"):
			# Yeah, creator only stuff, when I need to reset boombox and I'm away
			if (message.author.id == 350042456256937984): 
				await ch.send ("Restarting BOOMBOX.")
				print ("Restarting BOOMBOX")
				exit (0)
			else:
				await ch.send ("Unauthorized.")

		elif (len (client.voice_clients) == 0):
			await ch.send ("Nu sunt pe niciun canal de voce. Invita-ma folosind comanda **play**")

		elif (inp[0].lower () == "pauza"): await ch.send (engine.pause ())
		elif (inp[0].lower () == "continua"): await ch.send (engine.resume ())
		elif (inp[0].lower () == "coada" or inp[0].lower () == "c"): await ch.send (engine.getQueue ())
		elif (inp[0].lower () == "skip" or inp[0].lower () == "s"): await ch.send (engine.skip (inp[1] if len (inp) > 1 else 1))
		elif (inp[0].lower () == "seek"): await ch.send (engine.seek (inp[1]))
		elif (inp[0].lower () == "repeta" or inp[0].lower () == "r"): await ch.send (engine.repeat ())
		elif (inp[0].lower () == "stop"): await ch.send (engine.stop ())
		
		elif (inp[0].lower () == "flush"):
			os.system ("youtube-dl --rm-cache-dir")
			await ch.send ("Flushed youtube-dl cache.")
			
		elif (inp[0].isdigit ()):
			await ch.send (engine.chooseSearch (int (inp[0]), author))
		else:
			await ch.send ("Comanda necunoscuta: `{}`".format (inp[0]))

	except disnake.errors.ClientException as e:
		print ("Rebooting from Discord error:")
		print (e)
		exit ()

@client.event
async def on_ready ():
	print (client.user.name + " booted up.")
	await client.change_presence (activity = disnake.Game (name = ";;ajutor"))

try:
	token = open (utils.getPath () + "/token", "r").read ().strip ()
except:
	print ("Please create a 'token' file with your discord bot token in the same directory as the bot script.")
	exit (1)

os.system ("youtube-dl --rm-cache-dir")
client.run (token)